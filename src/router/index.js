import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import Collections from '@/components/Collections'
import Product from '@/components/Product'
import Cart from '@/components/Cart'
import Checkout from '@/components/Checkout'
import listOrder from '@/components/listOrder'
Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'HelloWorld',
      component: HelloWorld
    },
    {
      path: '/collections/:collectionId/:shopId',
      name: 'Collections',
      component: Collections
    },
    {
      path: '/product/:id/:shopId',
      name: 'Product',
      component: Product
    },
    {
      path: '/cart',
      name: 'Cart',
      component: Cart
    },
    {
      path: '/checkout/token=:token',
      name: 'Checkout',
      component: Checkout
    },
    {
      path: '/listorder',
      name: 'listOrder',
      component: listOrder
    },
  ]
})
