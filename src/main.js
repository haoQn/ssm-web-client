// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import Buefy from 'buefy'
import 'buefy/dist/buefy.css'
import * as VueGoogleMaps from "vue2-google-maps";
import ResetInput from 'v-reset-input'
import rate from 'vue-rate'
import 'vue-rate/dist/vue-rate.css'
import Vuesax from 'vuesax'

import 'vuesax/dist/vuesax.css' //Vuesax styles
Vue.use(Vuesax)
Vue.use(rate)
Vue.use(ResetInput)
Vue.use(VueGoogleMaps, {
  load: {
    key: "AIzaSyDtqlBL2XudSG3aIwHNNkBcj37CSjrFXqc",
    libraries: "places"
  }
});
Vue.use(Buefy,{
  defaultContainerElement:"body"
})
Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
